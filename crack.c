//3
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/types.h>
#include <sys/stat.h>

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


struct entry {
    char *hash;
    char *pass;
};


int bcompare(const void *a, const void *b){
    //printf("%s   %s\n",(*(struct entry *)a).hash, (*(struct entry *)b).hash);
    return strcmp(*(char **)a, *(char **)b);
}

int bs(const void *t, const void *a){
    return strcmp(t, *(char **)a);
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hash_the_guess = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strncmp(hash, hash_the_guess, HASH_LEN) == 0) return 1;
    else return 0;
    // Free any malloc'd memory
    free(hash_the_guess);
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
int file_length(char *filename){
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else
        return fileinfo.st_size;
}

struct entry *read_dictionary(char *filename, int *size){
    int len = file_length(filename);
    if (len == -1){
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    char *file_contents = malloc(len);
    
    FILE *fp = fopen(filename, "r");
    if (!fp){
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    int line_count = 0;
    for (int i = 0; i < len; i++){
        if (file_contents[i] == '\n'){
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    struct entry *bank = malloc((line_count) * sizeof(struct entry));
        printf("malloc=%lu, len=%d\n", (line_count) * sizeof(struct entry), len);
char **line = malloc(line_count * sizeof(char *));

    int c = 0;
    for (int i = 0; i < line_count; i++){
    line[i] = &file_contents[c];
    //bank[i].pass = &file_contents[i];
    //bank[i].hash = md5(bank[i].pass, strlen(bank[i].pass));
        while (file_contents[c] != '\0') c++;
        c++;
        
    *size = line_count;
    }
    


for (int i=0; i<line_count; i++){
    
    bank[i].pass = line[i];
    bank[i].hash = md5(bank[i].pass, strlen(bank[i].pass));
    //printf("%s\n", bank[i].hash);
    
}
free(line);
//printf("hi\n\n");
return bank;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    //printf("struct dict done");
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    
    qsort(dict, dlen, sizeof(struct entry), bcompare);
    //printf("sort works");
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    FILE *h_file = fopen(argv[1], "r");
    if (!h_file){
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    struct entry *found;
    char hash[HASH_LEN+1];
    while (fgets(hash, HASH_LEN+1, h_file) != NULL){
        hash[strlen(hash)-1]='\0';
        found = bsearch(hash, dict, dlen, sizeof(struct entry), bs);
        if (found != NULL){
            printf("%s is %s!\n", found->hash, found->pass);
        }
    }
    
}